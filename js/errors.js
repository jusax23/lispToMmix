import { error } from "./lexer.js"


export const argsCount = (name,num,pos)=>{
    error(`The native function '${name}' needs at least ${num} Arguments!`, ...pos);
}
export const argCount = (name,num,pos)=>{
    error(`The native function '${name}' needs ${num} Arguments!`, ...pos);
}
