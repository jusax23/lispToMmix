export default {
    assm: ({ execute, data, target, nid, ctx }) => {
        let cmds = data.array.slice(1, -1);
        let type = data.array.slice(-1)[0];
        return {
            code: "\n" + cmds.map(d => {
                let out = d.slice(1, -1);
                ([["?7", 7], ["?6", 6], ["?5", 5], ["?4", 4], ["?3", 3], ["?2", 2], ["?1", 1], ["?0", 0]]).map(a => {
                    out = out.split(a[0]).join("$" + (target + a[1]));
                });
                return out;
            }).join("\n") + "\n",
            type
        };
    },
    printRaw: ({ execute, data, target, nid, ctx }) => {
        let { code, type } = execute({ data: data[1], target: target });
        return {
            code: `${code}
        SET  $255,$${target}
        TRAP    0,Fputs,StdOut`,
            type
        };
    },
    addr: ({ execute, data, target, nid, ctx }) => {
        let nv = ctx.find(data[1], "V");
        if(ctx.local){
            return {
                code: `        LDOU $${target},HEAPpoint
        SET $${target + 1},${nv.pos()}
        ADDU  $${target},$${target},$${target + 1 }`,
                type: 0
            };
            
        }
        return {
            code: `        LDA  $${target},${nv.name}`,
            type: 0
        };
    },
    exit: () => {
        return {
            code: "        TRAP 0,Halt,0",
            type: 0
        }
    },
};