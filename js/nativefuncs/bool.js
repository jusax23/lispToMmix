import { argCount } from "../errors.js";
import { COMPUTE_TYPES, convertType, getOutType } from "../types.js";


export default {
    "<": ({ execute, data, target, nid, ctx }) => {
        if (data.length != 3) argCount("<",2,data.pos);
        let param1 = execute({ data: data[1], target});
        let param2 = execute({ data: data[2], target:target+1});
        let outType = getOutType(param1.type,param2.type);
        return {
            type: COMPUTE_TYPES.UINT,
            code: `${param1.code}
${convertType(param1.type, outType, target) }
${param2.code}
${convertType(param2.type, outType, target+1)}
        ${ (["CMPU", "CMP", "FCMP"])[outType] }  $${ target },$${ target },$${ target + 1 }
        ZSN   $${target},$${target},1`
        }
    },
    "<=": ({ execute, data, target, nid, ctx }) => {
        if (data.length != 3) argCount("<=",2,data.pos);
        let param1 = execute({ data: data[1], target});
        let param2 = execute({ data: data[2], target:target+1});
        let outType = getOutType(param1.type,param2.type);
        return {
            type: COMPUTE_TYPES.UINT,
            code: `${param1.code}
${convertType(param1.type, outType, target) }
${param2.code}
${convertType(param2.type, outType, target+1)}
        ${ (["CMPU", "CMP", "FCMP"])[outType] }  $${ target },$${ target },$${ target + 1 }
        ZSNP   $${target},$${target},1`
        }
    },
    "=": ({ execute, data, target, nid, ctx }) => {
        if (data.length != 3) argCount("=",2,data.pos);
        let param1 = execute({ data: data[1], target});
        let param2 = execute({ data: data[2], target:target+1});
        let outType = getOutType(param1.type,param2.type);
        return {
            type: COMPUTE_TYPES.UINT,
            code: `${param1.code}
${convertType(param1.type, outType, target) }
${param2.code}
${convertType(param2.type, outType, target+1)}
        ${ (["CMPU", "CMP", "FCMP"])[outType] }  $${ target },$${ target },$${ target + 1 }
        ZSZ   $${target},$${target},1`
        }
    },
    ">=": ({ execute, data, target, nid, ctx }) => {
        if (data.length != 3) argCount(">=",2,data.pos);
        let param1 = execute({ data: data[1], target});
        let param2 = execute({ data: data[2], target:target+1});
        let outType = getOutType(param1.type,param2.type);
        return {
            type: COMPUTE_TYPES.UINT,
            code: `${param1.code}
${convertType(param1.type, outType, target) }
${param2.code}
${convertType(param2.type, outType, target+1)}
        ${ (["CMPU", "CMP", "FCMP"])[outType] }  $${ target },$${ target },$${ target + 1 }
        ZSNN   $${target},$${target},1`
        }
    },
    ">": ({ execute, data, target, nid, ctx }) => {
        if (data.length != 3) argCount(">",2,data.pos);
        let param1 = execute({ data: data[1], target});
        let param2 = execute({ data: data[2], target:target+1});
        let outType = getOutType(param1.type,param2.type);
        return {
            type: COMPUTE_TYPES.UINT,
            code: `${param1.code}
${convertType(param1.type, outType, target) }
${param2.code}
${convertType(param2.type, outType, target+1)}
        ${ (["CMPU", "CMP", "FCMP"])[outType] }  $${ target },$${ target },$${ target + 1 }
        ZSP   $${target},$${target},1`
        }
    },
    "!=": ({ execute, data, target, nid, ctx }) => {
        if (data.length != 3) argCount("!=",2,data.pos);
        let param1 = execute({ data: data[1], target});
        let param2 = execute({ data: data[2], target:target+1});
        let outType = getOutType(param1.type,param2.type);
        return {
            type: COMPUTE_TYPES.UINT,
            code: `${param1.code}
${convertType(param1.type, outType, target) }
${param2.code}
${convertType(param2.type, outType, target+1)}
        ${ (["CMPU", "CMP", "FCMP"])[outType] }  $${ target },$${ target },$${ target + 1 }
        ZSNZ   $${target},$${target},1`
        }
    },
    "!": ({ execute, data, target, nid, ctx }) => {
        if (data.length != 2) argCount("!", 1, data.pos);
        let param = execute({ data: data[1], target });
        return {
            type: COMPUTE_TYPES.UINT,
            code: `${param.code}
        ${(["CMPU", "CMP", "FCMP"])[param.type]}  $${target},$${target},0
        ZSZ   $${target},$${target},1`
        }
    },
    "!!": ({ execute, data, target, nid, ctx }) => {
        if (data.length != 2) argCount("!!", 1, data.pos);
        let param = execute({ data: data[1], target });
        return {
            type: COMPUTE_TYPES.UINT,
            code: `${param.code}
        ${(["CMPU", "CMP", "FCMP"])[param.type]}  $${target},$${target},0
        ZSNZ   $${target},$${target},1`
        }
    },
};