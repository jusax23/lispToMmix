import { argsCount } from "../errors.js";
import { createType, error } from "../lexer.js";


export default {
    progn: ({ execute, data, target, nid, ctx }) => {
        if (data.length < 2) argsCount("progn", 1, data.pos);
        let newctx = ctx.nextLevel(false);
        let content = data.array.slice(1).map(d => execute({ data: d, target: target, ctx: newctx }).code).join("\n")
        return {
            type: 0,
            code: `//new code Block
${content}        
//end Code Block`
        }
    },
    defun: ({ execute, data, target, nid, ctx }) => {
        if(data.length < 5)argsCount("defun",4,...data[0].pos);
        let newctx = ctx.nextLevel(true);
        let type = ctx.getType(data[2]);
        let [argType,argsList] = createType(data[3]);
        if(argType!="code")error("The third Argument of defun must contain the args!",...data[3].pos);
        let args = [];
        if(argsList.length%2!=0)error("The Argument List must cotain an even amount of Names + Types",...data[3].pos);
        for (let i = 0; i < argsList.length; i+=2) {
            let name = argsList[i];
            let type = ctx.getType(argsList[i+1]);
            args.push(newctx.add({name,vType:"V",size:8,type:type.type}));  
                  
        }
        
        let fun = ctx.addFunction({name:data[1],code:"",type,args:args});            
        
        let code = data.array.slice(4).map(l=>{
            let {type,code} = execute({data:l,target:0,ctx:newctx});
            return code;
        }).join("\n");

        fun.code = code;
        fun.code += "\n        POP  1,0";

        return {
            code:"",
            type: 0
        };
    },
    return:({ execute, data, target, nid, ctx })=>{
        if(data.length == 1){
            return {
                type:0,
                code:`        POP   1,0
                `
            }
        }
        let {type,code} = execute({ data: data[1], target:0});
        return {
            type:0,
            code:`
${code}
        POP   1,0
            `
        }
    },
    if: ({ execute, data, target, nid, ctx }) => {
        if (data.length < 3) argsCount("if", 2, data.pos);
        let condition = execute({ data: data[1], target });
        let id1 = nid();
        let id2 = nid();
        if(data.length == 3){
            return {
                type: 0,
                code: `//if
${condition.code}
        BZ  $${target},fi${id1}
${execute({ data: data[2],target }).code}
fi${id1}    SWYM`
            }
        }
        return {
            type: 0,
            code: `//if
${condition.code}
        BZ  $${target},else${id1}
${execute({ data: data[2],target }).code}
        JMP  fi${id2} 
else${id1}  SWYM
${data.length > 3 ? execute({ data: data[2],target }).code : ""}
fi${id2}    SWYM`
        }
    },
};