import { createType, error, LISPcmd } from "../lexer.js";
import { COMPUTE_TYPES, convertType } from "../types.js";

const vars = {
    defvar: ({ execute, data, target, nid, ctx }) => {
        let param = data[3];
        let [type, d] = createType(param);

        let varType = ctx.getType(data[2]);
        if (varType.content) error("A variable only can be created with a primitive Type.", ...data[2].pos);

        if (type == "var" || type == "code" || ctx.local) {
            ctx.add({ name: data[1], vType: "V", size: varType.size, amount: 1, type: varType.type, content: 0 });
            let{code,type} = vars.set({execute,data:["set", data[1], data[3]],target,nid,ctx});
            return {
                code: code,
                type: type
            }
        } else {
            let content = "0";
            if(type == "num"){
                if(Number.isInteger(d)){
                    content = d;
                }else{
                    var buf = new ArrayBuffer(8);
                    (new Float64Array(buf))[0] = d;
                    let ddd = (new Uint32Array(buf));
                    content ="#";
                    content+=ddd[1].toString(16);
                    content+=ddd[0].toString(16);
                }
            }else{
                content=param;
            }
            ctx.add({ name: data[1], vType: "V", size: varType.size, amount: 1, type: varType.type, content });
            
            
            return {
                code: "",
                type: 0
            }
        }
    },
    set: ({ execute, data, target, nid, ctx }) => {
        let toSet = ctx.find(data[1],"V");
        let { code, type } = execute({ data: data[2], target });
        if(ctx.local){
            if (toSet.type == COMPUTE_TYPES.FLOAT) {
                return {
                    code: `${code}
${convertType(type, toSet.type, target)}
        LDOU $${target + 1},HEAPpoint   
        SET $${target + 2},${toSet.pos()}
        ${toSet.size > 4 ? "STOU" : "STSF"}    $${target},$${target+1},$${target + 2}`,
                    type: toSet.type
                };

            } else {
                return {
                    code: `${code}
${convertType(type, toSet.type, target)}
        LDOU $${target + 1},HEAPpoint     
        SET $${target + 2},${toSet.pos()}
        ST${("BWTTOOOO")[toSet.size - 1]}${toSet.type == 0 ? "U" : ""}    $${target},$${target + 1},$${target + 2}`,
                    type: toSet.type
                };
            }
        }else{
            if (toSet.type == COMPUTE_TYPES.FLOAT) {
                return {
                    code: `${code}
${convertType(type, toSet.type, target)}
        ${toSet.size > 4 ? "STOU" : "STSF"}    $${target},${toSet.name}`,
                    type: toSet.type
                };

            } else {
                return {
                    code: `${code}
${convertType(type, toSet.type, target)}
        ST${("BWTTOOOO")[toSet.size - 1]}${toSet.type == 0 ? "U" : ""}    $${target},${toSet.name}`,
                    type: toSet.type
                };
            }
        }
    },
    defarr: ({ execute, data, target, nid, ctx }) => {
        let param = data.array.slice(3, -1).map(d => Number(d));

        let varType = ctx.getType(data[2]);
        let amount = param.reduce((v, c) => v * c, 1);

        ctx.add({ name: data[1], vType:"V", size: varType.size, amount: amount, type: varType.type, config: param, content: data.array.slice(-1)[0] });

        return {
            code: "",
            type: 0
        };
    },
    readarr: ({ execute, data, target, nid, ctx })=>{
        let arr = ctx.find(data[1],"V");
        if (arr.config.length > data.length - 2) error(data[1] + " is a " + arr.config.length +"dim Array. You have to provide at least this much Arguments beside the Array name!",...data[1].pos);
        let params = data.array.slice(2).map((d, i) => execute({ data: d, target: target }));
        return{
            code:`
        
            `,
            type:arr.type
        }
    }
};

export default vars;