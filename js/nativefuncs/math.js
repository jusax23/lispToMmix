import { argCount, argsCount } from "../errors.js";
import { error } from "../lexer.js";
import { convertType, getOutType } from "../types.js"

export default {
    "+": ({ execute, data, target, nid, ctx }) => {
        if (data.length < 3) argsCount("+",2,data.pos);
        let params = data.array.slice(1).map((d, i) => execute({ data: d, target: target + (i==0 ? 0 : 1) }));
        let outType = getOutType(...params.map(d => d.type));
        return {
            type: outType,
            code: params.map((d, i) => {
                if (i == 0) return [
                    d.code,
                    convertType(d.type, outType, target)
                ];
                return [
                    d.code,
                    convertType(d.type, outType, target + 1),
                    `        ${(["ADDU", "ADD", "FADD"])[outType]}  $${target},$${target},$${target + 1}`
                ];
            }).flat(Infinity).join("\n")
        }
    },
    "-": ({ execute, data, target, nid, ctx }) => {
        if (data.length < 3) argsCount("-", 2, data.pos);
        let params = data.array.slice(1).map((d, i) => execute({ data: d, target: target + (i==0 ? 0 : 1) }));
        let outType = getOutType(...params.map(d => d.type));
        return {
            type: outType,
            code: params.map((d, i) => {
                if (i == 0) return [
                    d.code,
                    convertType(d.type, outType, target)
                ];
                return [
                    d.code,
                    convertType(d.type, outType, target + 1),
                    `        ${(["SUBU", "SUB", "FSUB"])[outType]}  $${target},$${target},$${target + 1}`
                ];
            }).flat(Infinity).join("\n")
        }
    },
    "*": ({ execute, data, target, nid, ctx }) => {
        if (data.length < 3) argsCount("*", 2, data.pos);
        let params = data.array.slice(1).map((d, i) => execute({ data: d, target: target + (i==0 ? 0 : 1) }));
        let outType = getOutType(...params.map(d => d.type));
        return {
            type: outType,
            code: params.map((d, i) => {
                if (i == 0) return [
                    d.code,
                    convertType(d.type, outType, target)
                ];
                return [
                    d.code,
                    convertType(d.type, outType, target + 1),
                    `        ${(["MULU", "MUL", "FMUL"])[outType]}  $${target},$${target},$${target + 1}`
                ];
            }).flat(Infinity).join("\n")
        }
    },
    "/": ({ execute, data, target, nid, ctx }) => {
        if (data.length < 3) argsCount("/", 2, data.pos);
        let params = data.array.slice(1).map((d, i) => execute({ data: d, target: target + (i==0 ? 0 : 1) }));
        let outType = getOutType(...params.map(d => d.type));
        return {
            type: outType,
            code: params.map((d, i) => {
                if (i == 0) return [
                    d.code,
                    convertType(d.type, outType, target)
                ];
                return [
                    d.code,
                    convertType(d.type, outType, target + 1),
                    `        ${(["DIVU", "DIV", "FDIV"])[outType]}  $${target},$${target},$${target + 1}`
                ];
            }).flat(Infinity).join("\n")
        }
    },
    "round": ({ execute, data, target, nid, ctx }) => {
        if (data.length < 2) argCount("round", 1, data.pos);
        let param = execute({ data: data[1], target: target });
        return {
            type: 1,
            code: `${param.code}
    ${convertType(param.type,2,target)}
    FINT $${target}$${target}`
        }
    },
    "sqrt": ({ execute, data, target, nid, ctx }) => {
        if (data.length < 2) argCount("sqrt", 1, data.pos);
        let param = execute({ data: data[1], target: target });
        return {
            type: 1,
            code: `${param.code}
    ${convertType(param.type,2,target)}
    FSQRT $${target}$${target}`
        }
    },
    "uint": ({ execute, data, target, nid, ctx }) => {
        if (data.length < 2) argCount("int", 1, data.pos);
        let param = execute({ data: data[1], target: target });
        return {
            type: 1,
            code: `${param.code}
    ${convertType(param.type,0,target)}`
        }
    },
    "int": ({ execute, data, target, nid, ctx }) => {
        if (data.length < 2) argCount("uint", 1, data.pos);
        let param = execute({ data: data[1], target: target });
        return {
            type: 1,
            code: `${param.code}
    ${convertType(param.type,1,target)}`
        }
    },
    "float": ({ execute, data, target, nid, ctx }) => {
        if (data.length < 2) argCount("float", 1, data.pos);
        let param = execute({ data: data[1], target: target });
        return {
            type: 1,
            code: `${param.code}
    ${convertType(param.type,2,target)}`
        }
    },
    "mod": ({ execute, data, target, nid, ctx }) => {
        if (data.length < 3) argCount("mod", 2, data.pos);
        let param1 = execute({ data: data[1], target });
        let param2 = execute({ data: data[2], target: target+1 });
        let outType = getOutType(param1.type,param2.type);
        let code = `${param1.code}
        ${convertType(param1.type,outType,target)}
${param2.code}
${convertType(param2.type,outType,target+1)}
`;
        if(outType == 0){
            code+=`        DIVU  $${target},$${target},$${target+1}
        GET     $${target},rR`;
        }else if(outType == 1){
            code+=`        DIV  $${target},${target},$${target+1}
        GET     $${target},rR`;
        }else if(outType == 2){
            code+=`        FREM $${target},$${target},$${target+1}`;
        }else{
            error("Unexpected Type: "+outType,...data[1].pos);
        }
        return{
            type:outType,
            code
        }
    },
}