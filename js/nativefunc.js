import bool from "./nativefuncs/bool.js";
import lanes from "./nativefuncs/lanes.js";
import math from "./nativefuncs/math.js";
import sys from "./nativefuncs/sys.js";
import vars from "./nativefuncs/vars.js";

export default {
    ...math,
    ...vars,
    ...lanes,
    ...sys, 
    ...bool,   
};