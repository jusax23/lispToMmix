import { error } from "./lexer.js";

export const COMPUTE_TYPES = {
    UINT: 0,
    INT: 1,
    FLOAT: 2
}

export const getOutType = (...types)=>Math.max(...types);

export const convertType = (typein,typeout,reg) => {
    if(typein == typeout) return "//no type conversion nessesary";
    if(typein == COMPUTE_TYPES.UINT && typeout == COMPUTE_TYPES.INT){
        return "//no type conversion nessesary (u -> i)";
    }
    if(typein == COMPUTE_TYPES.INT && typeout == COMPUTE_TYPES.UINT){
        return `        LDOU  $${reg + 1},intMask
        AND  $${reg},$${reg},$${reg + 1}    //convert i -> u`;
    }
    if(typein == COMPUTE_TYPES.INT && typeout == COMPUTE_TYPES.FLOAT){
        return `        FLOT  $${reg},$${reg}    //convert i -> f`;
    }
    if(typein == COMPUTE_TYPES.UINT && typeout == COMPUTE_TYPES.FLOAT){
        return `        LDOU  $${reg + 1},intMask
        AND  $${reg},$${reg},$${reg + 1}
        FLOT  $${reg},$${reg}    //convert u -> f`;
    }
    if(typein == COMPUTE_TYPES.FLOAT && typeout == COMPUTE_TYPES.INT){
        return `        FIX  $${reg},$${reg}    //convert f -> i`;
    }
    if(typein == COMPUTE_TYPES.FLOAT && typeout == COMPUTE_TYPES.UINT){
        return `        FIX  $${reg},$${reg}    //convert f -> u
        LDOU  $${reg + 1},intMask
        AND  $${reg},$${reg},$${reg+1}`;
    }
    error("[System error] Could not find a possible Type conversion. ("+typein+", "+typeout+")");
}

