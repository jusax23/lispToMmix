import { context } from "./ctx.js";
import { createType, error } from "./lexer.js";
import nativefunc from "./nativefunc.js";
import { COMPUTE_TYPES, convertType } from "./types.js";

let nid = 0;


export function execute({ data, target = 0, ctx = new context()}){
    if(target > 255) error("To much registers are required to run this. Support for this case will be build in later!",...data.pos);
    let [type, d] = createType(data);
    if(type == "code"){
        let userFunc = ctx.findFunction(data[0]);
        
        if(userFunc){
            let params = data.array.slice(1,userFunc.args.length+1).map((d, i) => execute({ data: d, target,ctx}));
            if(params.length<userFunc.args.length)error("function '"+data[0]+"' is called with to less Arguments!",...data[data.length-1].pos);
            let ctxSize = ctx.size();
            let code = `
${params.map((p,i)=>{
    let arg = userFunc.args[i];
    return [
        p.code,
        convertType(p.type,arg.type,target),
        `        LDOU   $${target + 1},HEAPpoint`,
        `        SET   $${target + 2},${ctxSize+arg.pos()}`,
        `        ${arg.type == COMPUTE_TYPES.FLOAT?(arg.size > 4 ? "STOU" : "STSF"):("ST"+("BWTTOOOO")[arg.size - 1])+(arg.type == 0 ? "U" : "")}    $${target},$${target + 1},$${target + 2}`
    ]
}).flat(Infinity).join("\n")}
        
        LDOU   $${target},HEAPpoint
        ADDU   $${target},$${target},${ctxSize}
        STOU   $${target},HEAPpoint

        GET    $${target},rJ
        PUSHJ  $${target+1},${userFunc.name}
        PUT    rJ,$${target}

        LDOU   $${target},HEAPpoint
        SUBU   $${target},$${target},${ctxSize}
        STOU   $${target},HEAPpoint

        SET    $${target},$${target+1}
            `;
            return {
                code,
                type:userFunc.type.type
            }
        }
        try {
            let { type, code } = nativefunc[data[0]]({ 
                execute: ({ data, target = 0, ctx:contx = ctx }) => execute({ data, target, ctx:contx }), 
                data, 
                target, 
                nid: () => nid++, 
                ctx 
            });
            return {code,type};
        } catch (e) {
            console.log(e);
            error(`'${data[0]}' is not a function`,...data.pos);
        }
    }else if (type == "var"){
        let { size, amount, type, name,pos } = ctx.find(d,"V",data.pos);
        if(ctx.local){
            if (size <= 8 && amount == 1) {
                if (type == COMPUTE_TYPES.FLOAT) {
                    return {
                        type: 2,
                        code: `        LDOU $${target},HEAPpoint
        SET $${target + 1},${pos()}
        ${size > 4 ? "LDOU" : "LDSF"}  $${target},$${target},$${target+1}`
                    }
                } else {
                    return {
                        type: type,
                        code: `        LDOU $${target },HEAPpoint
        SET $${target + 1},${pos()}     
        LD${("BWTTOOOO")[size - 1]}${type == 0 ? "U" : ""}  $${target},$${target},$${target + 1}`
                    }
                }
            } else {
                return {
                    type: 0,
                    code: `        LDOU $${target},HEAPpoint
        SET $${target + 1},${pos()}
        ADDU  $${target},$${target},$${target + 1}`
                }
            }
        }else{
            if (size <= 8 && amount == 1) {
                if (type == COMPUTE_TYPES.FLOAT) {
                    return {
                        type: 2,
                        code: `        ${size > 4 ? "LDOU" : "LDSF"}  $${target},${name}`
                    }
                } else {
                    return {
                        type: type,
                        code: `        LD${("BWTTOOOO")[size - 1]}${type == 0 ? "U" : ""}  $${target},${name}`
                    }
                }
            } else {
                return {
                    type: 0,
                    code: `        LDA  $${target},${name}`
                }
            }
        }
        
        
    }else if (type == "bool"){
        return {
            code: `        SET  $${target},${d}`,
            type: COMPUTE_TYPES.UINT
        }
    }else if (type == "num"){
        let hex = "";
        if(Number.isInteger(d)){
            hex = d;
          }else{
            var buf = new ArrayBuffer(8);
            (new Float64Array(buf))[0] = d;
            let ddd = (new Uint32Array(buf));
            hex ="#";
            hex+=ddd[0].toString(16);
            hex+=ddd[1].toString(16);
          }
  
        return {
            code: `        SET  $${target},${hex}`,
            type: COMPUTE_TYPES.UINT
        }
    }else if (type == "str"){
        
        return {
            code: `        SET  $${target},"${d}"`,
            type: COMPUTE_TYPES.UINT
        }
    }
    throw new Error("nothing found");
}