import * as path from "path";

function join(p1,p2){
    if(p2.startsWith("/"))return p2;
    return path.join(p1,"..",p2);
}
function unComment(str){
    let out = "";
    let inK = false;
    for (let i = 0; i < str.length; i++) {
        if(str[i] == "\\"){
            out+=str[i+1]??"";
            i+=2;
        }else if(str[i] == "\""){
            inK = !inK;
            out += str[i];
        }else if(str[i] == ";" || str[i] == " "){
            if(inK)out += str[i];
            else break;
        }else{
            out += str[i];
        }
    }
    return out.trim();
}

export default (str,filename,readFile)=>str.split("\n").map(d=>d.trim()).map((l,i)=>{
    if(!l.startsWith("#"))return l;
    if(l.startsWith("#import")){
        let toImport = unComment(l.substring(8).trim());
        if(toImport.startsWith("\"") && toImport.startsWith("\"")){
            return readFile(join(filename,toImport.slice(1,-1)),i);
        }else{
            return readFile("lisp/"+toImport+".lisp",i);
        }
    }
}).join("\n");