import * as fs from "fs";
import { context } from "./js/ctx.js";
import { execute } from "./js/execute.js";
import { LISPcmd, LISPstring, createType, error } from "./js/lexer.js";
import preprocessor from "./js/preprocessor.js";
var path = process.argv[2];
var pathout = process.argv[3];
if (!path || !pathout) {
    console.log("PLease this Schema: node ToAs.js [path] [pathout]");
    process.exit();
}
var file = fs.readFileSync(path).toString();

let strcode = preprocessor(file,path,(path,line)=>{
    try{
        return fs.readFileSync(path).toString();
    }catch(_){
        error("Can not import file: "+path+" -> "+_, line, 0);
    }
});
var data = new LISPcmd("(\n" + strcode + "\n)", 0, 0);


let code = "";
var ctx = new context();
for (var i = 0; i < data.length; i++) {
    let { code: c, type } = execute({ data: data[i], ctx });
    if(c=="")continue;
    code += c+"\n";
}

let result = ctx.build();
result+=code;
result+=ctx.buildFunctions();
fs.writeFileSync(pathout, result);
console.log(`Finished compiling in ${Math.round(performance.now()) / 1000}sec. Assembly saved to: ${pathout}`);
