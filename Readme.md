# Lisp to MMIX Commpiler
This is a simple Commpiler vor lisp to mmix. It uses a custom lisp flavour.

## Features
### Import
```lisp
#import math ;import default library
#import "some.lisp" ;import your own lisp
```
### Variables
```lisp
(defvar [name]:[type] [value])
(set [name] [value])
```
### Math
```lisp
(+ [value] [value] ...)
(- [value] [value] ...)
(* [value] [value] ...)
(/ [value] [value] ...)
(mod [value] [value])
(round [value])
(sqrt [value])
(uint [value])
(int [value])
(float [value])
```
### Bools and Comparison
```lisp
(< [value] [value])
(<= [value] [value])
(= [value] [value])
(>= [value] [value])
(> [value] [value])
(! [value])
(!! [value])
```
### Branches, Loops, Funktions
```lisp
(progn [command] ...)

(if [condition] [command])
(if [condition] [command] [command]) ; then and else

(defun [name]:[type] 
    [command]
    ...
    (return [value])
)

```
### Rest
```lisp
(assm [string])

(printRaw [addr])

(addr [variable])

(exit)
```